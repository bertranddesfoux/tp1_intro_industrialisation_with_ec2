import boto3

def put_user(UniqID, name, surname, email, phone, dynamodb=None):
    if not dynamodb:
        dynamodb = boto3.resource('dynamodb')

    table = dynamodb.Table('DynamoDB.bertrand.EC2.com')
    response = table.put_item(
        Item={
            'UniqID': UniqID,
            'name': name,
            'surname': surname,
            'email': email,
            'phone': phone
        }
    )
    return 0

if __name__ == '__main__':
    put_user(1, "jean", "dupont", "duponjean@aws.com", "+33659894522")